package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        // TODO: Implement the logic here
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        boolean answer = false;
        List temp = y;
        for (Object originalObject :
                x) {
            temp = findElement(originalObject, temp);

        }
        if (temp != null) {answer = true;}
        return answer;
    }

    public List findElement (Object o, List l) {
        List newList = null;
        try {
            for (Object listObject :
                    l) {
                if (o.equals(listObject)) {
                    newList = l.subList(l.indexOf(listObject), l.size());
                    break;
                }
            }
        } catch (NullPointerException e) {
            return null;
        }
        return newList;
    }
}
