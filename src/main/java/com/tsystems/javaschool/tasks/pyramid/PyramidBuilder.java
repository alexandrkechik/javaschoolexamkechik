package com.tsystems.javaschool.tasks.pyramid;

import java.util.HashSet;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        try{inputNumbers.sort(Integer::compareTo);
        } catch (Throwable e) {throw new CannotBuildPyramidException();}
        /*В формулировке задания было не совсем ясно указано, допускает ли
        создание пирамиды наличие одинаковых значений в листе, поэтому
        я добавил этот метод, проверяющий уникальность листа. Если же
        одинаковые значения возможны, то его можно просто удалить
         */

        if (!uniquenessCheck(inputNumbers)) {
            throw new CannotBuildPyramidException();
        }
        Integer[] pyramidData = getPyramidData(inputNumbers);
        return createPyramid(inputNumbers, pyramidData);
    }
    /*Проверяем достаточно ли в листе чисел для создания пирамиды,
     а также получаем размер пирамиды, если создание возможно*/
    private Integer[] getPyramidData(List<Integer> inputNumbers){
        int size = inputNumbers.size();
        int answer = 0;
        int width = 1;
        int height = 0;
        int numbersInRow = 1;
        for (int i = 1; i <= size; i = i + numbersInRow) {
            height++;
            numbersInRow++;
            if (i == size) {answer = 1; break;}
            width = width+2;
        }
        if (answer != 1) {
            throw new CannotBuildPyramidException();
        }
        return new Integer[]{height, width};
    }
    //заполняем пирамиду числами из листа
    private static int[][] createPyramid(List<Integer> listOfNumbers, Integer[] pyramidData) {
        int height = pyramidData[0];
        int width = pyramidData[1];
        int center = width/2;
        int[][] pyramid = new int[height][width];
        //int rowNumber = 0;
        int listElement = 0;
        int counter = 0;
        HashSet<Integer> vacantPositions1 = new HashSet<>();
        HashSet<Integer> vacantPositions2 = new HashSet<>();
        for (int rowNumber = 0; rowNumber < pyramid.length; rowNumber++) {
            int[] row = pyramid[rowNumber];
            if (rowNumber == 0|| rowNumber % 2 == 0){
                vacantPositions1.add(center + counter);
                vacantPositions1.add(center - counter);
                for (int i:
                        vacantPositions1) {
                    pyramid[rowNumber][i] = listOfNumbers.get(listElement);
                    listElement++;
                }
            }else{
                vacantPositions2.add(center + counter);
                vacantPositions2.add(center - counter);
                for (int i:
                        vacantPositions2) {
                    pyramid[rowNumber][i] = listOfNumbers.get(listElement);
                    listElement++;
                }
            }
            counter++;
        }
        return pyramid;
    }

    private static boolean uniquenessCheck(List<Integer> listOfNumbers) {
        int checker = 0;
        boolean isUnique = true;
        for (int i = 0; i < listOfNumbers.size(); i++) {
            int currentNumber = listOfNumbers.get(i);
            if (checker == currentNumber && i != 0) {
                isUnique = false;
            }
            checker = currentNumber;
        }
        return isUnique;
    }


}
